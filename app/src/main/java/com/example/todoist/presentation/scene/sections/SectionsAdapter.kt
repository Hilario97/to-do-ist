package com.example.todoist.presentation.scene.sections

import android.view.View
import com.example.todoist.R
import com.example.todoist.data.model.Section
import com.example.todoist.databinding.SimpleTextItemBinding
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.viewbinding.BindableItem

class SectionsAdapter : GroupAdapter<GroupieViewHolder>() {
    fun setItems(sectionList: List<Section>) {
        sectionList.forEach { add(SectionsAdapter.SectionItem(it)) }
    }

    internal class SectionItem(private val section: Section) :
        BindableItem<SimpleTextItemBinding>() {
        override fun bind(viewBinding: SimpleTextItemBinding, position: Int) {
            viewBinding.simpleTextItem.text = section.name
        }

        override fun getLayout(): Int = R.layout.simple_text_item

        override fun initializeViewBinding(view: View): SimpleTextItemBinding =
            SimpleTextItemBinding.bind(view)
    }
}